
## Release notes

### Version 1.1.1, 07-2022
- some improvements to param searching
- default apps foder moved to "UserDir/boaks/..." (can be changed in settings.py)

### Version 1.1.0, 05-2022
- some improvements to param searching
- param .yml file inside package can now be named flexibly but must be prefixed with 'boak'
- see settings.py pgParamPrefix to change prefix

### Version 1.0.9, 05-2022
- bugfix false executable

### Version 1.0.8, 05-2022
- python requirement changed to 3.9 and higher
- run result can now use logging
- use of external file cmds

### Version 1.0.7, 05-2022
- python requirement changed to 3.8 and higher
- Pipenv inside package now allowed (.venv folder inside package)

### Version 1.0.6, 05-2022
- Bugfix for Error verifying correct executable (second try)
- Bugfix os.sep error

### Version 1.0.5, 05-2022
- Bugfix for Error verifying correct executable (file not found error)
- app.yml file can now be held within the app directory as boakboak.yml

### Version 1.0.4, 05-2022
- last known executable is automatically added to apps.yml files

### Version 1.0.3, 05-2022
- some technical changes, i.e. README.md

### Version 1.0.0, 05-2022
- python 3.8 - 3.10
- venvs: pipenv
- os: tested on Windows 10